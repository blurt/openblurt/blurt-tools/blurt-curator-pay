require('dotenv').config()
const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const CronJob = require('cron').CronJob;
const config = require('./config.json')

const CRON_SCHED = process.env.CRON_SCHED || "00 01 00 * * *";
const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
const CANCEL_PD_ON_START = process.env.CANCEL_PD_ON_START === "true";

blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

let keys_map = {}
extractKeys(keys_map)

// Create accounts map
let accounts_map = {}
config.accounts.forEach(item => {
  // check if key defined in .env
  if (keys_map[item.name]) {
    accounts_map[item.name] = {
      wif: keys_map[item.name],
      routes: item.routes,
    }
  }
});

// Cancel power down
let accounts = Object.keys(keys_map);
if (CANCEL_PD_ON_START) {
  blurt.api.getAccounts(accounts, function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(account => {
        let { vesting_withdraw_rate } = account;
        if (vesting_withdraw_rate !== "0.000000 VESTS") {
          cancelPowerDown(account);
        }
      })
    }
  });
}

const job = new CronJob(CRON_SCHED, function() {
  blurt.api.getAccounts(accounts, function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(async account => {
        // check if current power down has not liquidated (transferred yet)
        let now = moment.utc();
        console.log('now', now)

        let { next_vesting_withdrawal } = account;
        let withdrawDate = moment.utc(next_vesting_withdrawal);
        console.log('withdrawDate', withdrawDate)

        // compute for delay until withdrawal is executed;
        // (next_vesting_withdrawal - now) => convert diff to ms
        let timedelay = withdrawDate.diff(now);
        console.log('timedelay', timedelay)

        // don't broadcast next powerdown until current powerdown liquid is executed
        if (timedelay > 0) {
          setTimeout(function() {
            powerDown(account);
          }, timedelay)
        } else {
          powerDown(account);
        }

      })
    }
  });
});
job.start();

// Extract keys from .env
function extractKeys(map) {
  const ACCOUNTS_COUNT = parseInt(process.env.ACCOUNTS_COUNT || 19);
  for (let i = 0; i < ACCOUNTS_COUNT; i++) {
    let c = `C${i.toString().padStart(2, '0')}`;
    if (process.env[c]) {
      let entry = process.env[c].split(",")
      map[entry[0].trim()] = entry[1].trim()
    }
  }
}

function cancelPowerDown(account) {
  let { name } = account;
  let { wif } = accounts_map[account.name];
  // cancel power down
  blurt.broadcast.withdrawVesting(wif, name, '0.000000 VESTS',
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      console.log(`cancelled power down for ${name}`)
    }
  );
}

function powerDown(account) {
  let { name } = account;
  console.log('name', name)
  console.log('accounts_map', accounts_map)
  let { wif, routes } = accounts_map[name];
  let vesting_shares = account.vesting_shares.replace(" VESTS", "").replace(".", "");
  let delegated_vesting_shares = account.delegated_vesting_shares.replace(" VESTS", "").replace(".", "");
  let withdrawable_vests = (vesting_shares - delegated_vesting_shares) / 1e6
  let to_powerdown_vests = withdrawable_vests.toString() + ' VESTS'

  // power down
  blurt.broadcast.withdrawVesting(wif, name, to_powerdown_vests,
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      console.log(`powered down ${withdrawable_vests}`)

      // perform transfers
      routes.forEach(route => {
        let { to_account, share } = route;
        let to_transfer = (withdrawable_vests * (share / 100)).toFixed(3);
        console.log('to_transfer', to_transfer)
        setTimeout(function() {
          blurt.broadcast.transfer(
            wif, name, to_account, `${to_transfer} BLURT`,
            `Curator pay ${to_transfer} BLURT`,
            function(err, result) {
              console.log(err, result);
            }
          );
        }, 1000 * 60 *  24 * 7) // 1sec * 60 minutes * 24 hours * 7 days
      })
    }
  );
}