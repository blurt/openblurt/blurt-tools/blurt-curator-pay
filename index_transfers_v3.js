require('dotenv').config()
const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const config = require('./config.json')

const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
const CANCEL_PD_ON_START = process.env.CANCEL_PD_ON_START === "true";
// (1sec * 60 minutes * 24 hours * 7 days ) + 3 secs
const TRANSFER_TIMEOUT = parseInt(process.env.TRANSFER_TIMEOUT) || (1000 * 60 * 60 * 24 * 7);
const FIRST_POWERDOWN_DELAY = parseInt(process.env.FIRST_POWERDOWN_DELAY) || 3000;

blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

let keys_map = {}
extractKeys(keys_map)

// Create accounts map
let accounts_map = {}
config.accounts.forEach(item => {
  // check if key defined in .env
  if (keys_map[item.name]) {
    accounts_map[item.name] = {
      wif: keys_map[item.name],
      routes: item.routes,
    }
  }
});

// Cancel power down
let accounts = Object.keys(keys_map);
if (CANCEL_PD_ON_START) {
  blurt.api.getAccounts(accounts, function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(account => {
        let { vesting_withdraw_rate } = account;
        if (vesting_withdraw_rate !== "0.000000 VESTS") {
          cancelPowerDown(account);
        }
      })
    }
  });
}

blurt.api.getAccounts(accounts, function (err, result) {
  if (!_.isEmpty(result)) {
    result.forEach(async account => {
      let { name } = account;
      let { wif, routes } = accounts_map[name];
      setTimeout(powerDown.bind(null, account, wif, routes), FIRST_POWERDOWN_DELAY);
    })
  }
});

// Extract keys from .env
function extractKeys(map) {
  const ACCOUNTS_COUNT = parseInt(process.env.ACCOUNTS_COUNT || 19);
  for (let i = 0; i < ACCOUNTS_COUNT; i++) {
    let c = `C${i.toString().padStart(2, '0')}`;
    if (process.env[c]) {
      let entry = process.env[c].split(",")
      map[entry[0].trim()] = entry[1].trim()
    }
  }
}

function cancelPowerDown(account) {
  let { name } = account;
  let { wif } = accounts_map[account.name];
  // cancel power down
  blurt.broadcast.withdrawVesting(wif, name, '0.000000 VESTS',
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      console.log(`cancelled power down for ${name}`)
    }
  );
}

function powerDown(account, wif, routes) {
  let { name } = account;
  let vesting_shares = account.vesting_shares.replace(" VESTS", "").replace(".", "");
  let delegated_vesting_shares = account.delegated_vesting_shares.replace(" VESTS", "").replace(".", "");
  let withdrawable_vests = ((vesting_shares - delegated_vesting_shares) / 1e6).toFixed(6)
  let to_powerdown_vests = withdrawable_vests.toString() + ' VESTS'

  // power down
  console.log('to_powerdown_vests', to_powerdown_vests)
  blurt.broadcast.withdrawVesting(wif, name, to_powerdown_vests,
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      console.log(`powered down ${withdrawable_vests}`)
      setTimeout(doTransfers.bind(null, account, wif, routes, withdrawable_vests), TRANSFER_TIMEOUT + 3000) // (1sec * 60 minutes * 24 hours * 7 days ) + 3 secs
    }
  );
}

function doTransfers(account, wif, routes, withdrawable_vests) {
  let { name } = account;
  // perform transfers
  routes.forEach((route, index) => {
    let { to_account, share } = route;
    // less 0.200 BLURT for transaction fee
    let to_transfer = ((withdrawable_vests / 13) * (share / 100)).toFixed(3) - 0.100;
    console.log('to_transfer', to_transfer)

    blurt.broadcast.transfer(
      wif, name, to_account, `${to_transfer} BLURT`,
      `Curator pay ${to_transfer} BLURT`,
      function(err, result) {
        console.log(err, result);
      }
    );

    /*
    if (routes.length === index) {
      powerDown(account, wif, routes);
    }
    */
  })
}
