require('dotenv').config()
const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const mongoose = require('mongoose');
const CronJob = require('cron').CronJob;
const config = require('./config.json')

const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
const EVERY_MINUTE = "0 * * * * *";
const EVERY_2_MINUTES = "0 */2 * * * *";
const EVERY_3_MINUTES = "0 */3 * * * *";
const EVERY_HOUR = "0 0 * * * *";
const PD_WEEK_COUNT = parseInt(process.env.PD_WEEK_COUNT) || 4;

blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const { CuratorPaySchema } = require('./src/schemas');
const CuratorPay = db1.model('CuratorPay', CuratorPaySchema);

let keys_map = {}
extractKeys(keys_map)

// Create accounts map
let accounts_map = {}
config.accounts.forEach(item => {
  // check if key defined in .env
  if (keys_map[item.name]) {
    accounts_map[item.name] = {
      wif: keys_map[item.name],
      routes: item.routes,
    }
  }
});
let accounts = Object.keys(keys_map);

// POWER DOWN CronJob
const pd_job = new CronJob(EVERY_3_MINUTES, function() {
  blurt.api.getAccounts(accounts, function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(async account => {
        let { name, vesting_withdraw_rate } = account;
        let { wif, routes } = accounts_map[name];

        let is_powering_down = vesting_withdraw_rate !== "0.000000 VESTS";
        if (!is_powering_down) {
          powerDown(account, wif, routes);
        }
      })
    }
  });
});
pd_job.start();

// Curator Pay Transfer CronJob
const transfer_job = new CronJob(EVERY_MINUTE, async function() {
  blurt.api.getAccounts(accounts, async function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(async account => {
        let { wif } = accounts_map[account.name];
        const curator_pays = await CuratorPay.find({
          from_account: account.name,
          status: "open",
          payout_date: { $lte: new Date() }
        });
        console.log('to transfer curator pays', curator_pays);
        if (curator_pays && curator_pays.length > 0) {
          doTransfers(wif, curator_pays);
        }
      })
    }
  });
});
transfer_job.start()

// Cancel POWER DOWN CronJob
const cancel_pd_job = new CronJob(EVERY_2_MINUTES, function() {
  blurt.api.getAccounts(accounts, function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(async account => {
        const curator_pays = await CuratorPay.find({
          from_account: account.name,
          status: "open",
        });
        let { vesting_withdraw_rate } = account;
        let { wif } = accounts_map[account.name];

        let all_pays_paid = curator_pays.length === 0;
        let is_powering_down = vesting_withdraw_rate !== "0.000000 VESTS";
        if (all_pays_paid && is_powering_down) {
          cancelPowerDown(account, wif);
        }
      })
    }
  });
});
cancel_pd_job.start();

// Extract keys from .env
function extractKeys(map) {
  const ACCOUNTS_COUNT = parseInt(process.env.ACCOUNTS_COUNT || 19);
  for (let i = 0; i < ACCOUNTS_COUNT; i++) {
    let c = `C${i.toString().padStart(2, '0')}`;
    if (process.env[c]) {
      let entry = process.env[c].split(",")
      map[entry[0].trim()] = entry[1].trim()
    }
  }
}

function powerDown(account, wif, routes) {
  let { name } = account;
  let vesting_shares = account.vesting_shares.replace(" VESTS", "").replace(".", "");
  let delegated_vesting_shares = account.delegated_vesting_shares.replace(" VESTS", "").replace(".", "");
  let withdrawable_vests = ((vesting_shares - delegated_vesting_shares) / 1e6).toFixed(6)
  let to_powerdown_vests = withdrawable_vests.toString() + ' VESTS'

  // power down
  console.log('to_powerdown_vests', to_powerdown_vests)
  blurt.broadcast.withdrawVesting(wif, name, to_powerdown_vests,
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      console.log(`powered down ${withdrawable_vests}`)
      doRegisterCuratorPay(account, wif, routes, withdrawable_vests)
    }
  );
}

async function doRegisterCuratorPay(account, wif, routes, withdrawable_vests) {
  let account_info = await blurt.api.getAccountsAsync([account.name]);
  // register transfers
  routes.forEach(async (route) => {
    let { to_account, share } = route;
    // less 0.100 BLURT for transaction fee
    let to_transfer = ((withdrawable_vests / PD_WEEK_COUNT) * (share / 100)).toFixed(3) - 0.100;
    console.log('registered curator pay: to_transfer', to_transfer)
    let memo = `Curator Pay ${to_transfer}`;

    let item = new CuratorPay();
    let payout_date = new Date(account_info[0].next_vesting_withdrawal);
    payout_date.setHours(23,59,59,999);

    let yyyy = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(payout_date);
    let mm = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(payout_date);
    let dd = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(payout_date);
    _.extend(item, {
      from_account: account.name,
      to_account: to_account,
      amount: to_transfer,
      memo: memo,
      status: "open",
      payout_date: payout_date,
      payout_date_str: `${yyyy}${mm}${dd}`,
      timestamp: new Date(),
    });
    await item.save();
  })
}

function doTransfers(wif, curator_pays) {
  curator_pays.forEach(async (curator_pay) => {
    let { from_account, to_account, amount } = curator_pay;
    blurt.broadcast.transferAsync(
      wif, from_account, to_account, `${amount} BLURT`,
      `Curator pay ${amount} BLURT`,
    ).then(async () => {
      curator_pay.status = "closed";
      await curator_pay.save();
    }).catch(error => {
      console.error('error', error)
    });
  })
}

function cancelPowerDown(account, wif) {
  let { name } = account;
  // cancel power down
  blurt.broadcast.withdrawVesting(wif, name, '0.000000 VESTS',
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      console.log(`cancelled power down for ${name}`)
    }
  );
}