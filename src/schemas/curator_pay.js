const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CuratorPaySchema = new Schema({
  from_account: {
    type: String,
    required : true,
  },
  to_account: {
    type: String,
    required : true,
  },
  amount: {
    type: Number,
    required : true,
  },
  powerdown_date: {
    type: Date,
  },
  payout_date: {
    type: Date,
  },
  payout_date_str: {
    type: String,
  },
  status: {
    type: String,
  },
  timestamp: {
    type: Date,
    expires: '8d',
    default: Date.now
  },
});

CuratorPaySchema.index({ from_account: 1, to_account: 1, payout_date_str: 1 }, { unique: true })

module.exports = { CuratorPaySchema };