const { CuratorPaySchema } = require('./curator_pay');
const { CuratorRouteSchema } = require('./curator_pay');

module.exports = {
  CuratorPaySchema,
  CuratorRouteSchema,
};